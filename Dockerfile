FROM richarvey/nginx-php-fpm

# remove previous default files
RUN rm -rf /var/www/html

# clone kimai to this path instead (--depth 1 ensures that only the last comit gets pulled and not the whole history)
RUN git clone -b 1.3.1 --depth 1 https://github.com/kimai/kimai.git /var/www/html/

# add a script that can automatically delete the installation folder and fixes permissions
COPY entrypoint.sh /entrypoint.sh

# entrypoint is the first thing that get run when starting a container
ENTRYPOINT ["/entrypoint.sh"]

# CMD is needed here again, even if it is already in the same in "richarvey/nginx-php-fpm". Otherwise 'exec "$@"' of entrypoint.sh to continue the docker command will not work and the container would just exit
CMD ["/start.sh"]