#!/bin/bash

# The following is to get rid of the warning message after installation 
# Remove installation directory on container startup if autoconf.php file already exists.
if [ -e /var/www/html/includes/autoconf.php ]
then
  echo -n "=> Remove Kimai installation directory ... "
  rm -rf /var/www/html/installer
  echo "done"
fi


# this will continue running the docker command. (Will execute the command specified with CMD after ENTRYPOINT of the Dockerfile)
exec "$@"